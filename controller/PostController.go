package controller

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"log"
	"oceanlearn.tech/ginessential/common"
	"oceanlearn.tech/ginessential/model"
	"oceanlearn.tech/ginessential/response"
	"oceanlearn.tech/ginessential/vo"
	"strconv"
)

type IPostController interface {
	RestController
	PageList(ctx *gin.Context)
}

type PostController struct {
	DB *gorm.DB
}

func NewPostController() IPostController {
	db := common.GetDB()
	db.AutoMigrate(model.Post{})
	return PostController{DB: db}
}

func (p PostController) Create(ctx *gin.Context) {
	var requestPost vo.CreatePostRequest
	// 数据验证
	if err := ctx.ShouldBind(&requestPost); err != nil {
		log.Println(err.Error())
		response.Fail(ctx, nil, "数据验证错误")
		return
	}

	// 获取登录用户user
	user, _ := ctx.Get("user")

	// 创建post
	post := model.Post{
		UserID:     user.(model.User).ID,
		CategoryId: requestPost.CategoryId,
		Title:      requestPost.Title,
		HeadImg:    requestPost.HeadImg,
		Content:    requestPost.Content,
	}

	if err := p.DB.Create(&post).Error; err != nil {
		panic(err)
		return
	}

	response.Success(ctx, gin.H{"post": post}, "创建成功")
}

func (p PostController) Update(ctx *gin.Context) {
	var requestPost vo.CreatePostRequest
	// 数据验证
	if err := ctx.ShouldBind(&requestPost); err != nil {
		log.Println(err.Error())
		response.Fail(ctx, nil, "数据验证错误")
		return
	}

	// 获取path中的id
	postId := ctx.Params.ByName("id")
	var post model.Post
	if err := p.DB.Where("id = ?", postId).First(&post).Error; err != nil {
		response.Fail(ctx, nil, "文章不存在")
		return
	}

	// 判断当前用户是否为要修改文章的作者
	user, _ := ctx.Get("user")
	userId := user.(model.User).ID
	if userId != post.UserID {
		response.Fail(ctx, nil, "文章不属于您，请不要非法操作")
		return
	}

	// 更新文章
	if err := p.DB.Model(&post).Updates(requestPost).Error; err != nil {
		panic(err)
		return
	}

	response.Success(ctx, gin.H{"post": post}, "更新成功")
}

func (p PostController) Show(ctx *gin.Context) {
	// 获取path中的id
	postId := ctx.Params.ByName("id")
	var post model.Post
	// 关联查询出category的内容
	if err := p.DB.Preload("Category").Where("id = ?", postId).First(&post).Error; err != nil {
		response.Fail(ctx, nil, "文章不存在")
		return
	}

	response.Success(ctx, gin.H{"post": post}, "查询成功")
}

func (p PostController) Delete(ctx *gin.Context) {
	// 获取path中的参数
	postId := ctx.Params.ByName("id")

	var post model.Post

	// 判断文章是否存在
	if err := p.DB.Where("id = ?", postId).First(&post).Error; err != nil {
		response.Fail(ctx, nil, "文章不存在")
		return
	}

	// 判断当前用户是否为要修改文章的作者
	user, _ := ctx.Get("user")
	userId := user.(model.User).ID
	if userId != post.UserID {
		response.Fail(ctx, nil, "文章不属于您，请不要非法操作")
		return
	}

	if err := p.DB.Delete(&post).Error; err != nil {
		panic(err)
		return
	}

	response.Success(ctx, gin.H{"post": post}, "删除成功")
}

func (p PostController) PageList(ctx *gin.Context) {
	// 获取分页参数
	pageNum, _ := strconv.Atoi(ctx.DefaultQuery("pageNum", "1"))
	pageSize, _ := strconv.Atoi(ctx.DefaultQuery("pageSize", "20"))

	// 分页
	var posts []model.Post
	p.DB.Offset((pageNum - 1) * pageSize).Limit(pageSize).Find(&posts)

	// 总条数
	var total int64
	p.DB.Model(model.Post{}).Count(&total)
	response.Success(ctx, gin.H{"data": posts, "total": total}, "查询成功")
}
