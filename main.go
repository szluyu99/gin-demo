package main

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"oceanlearn.tech/ginessential/common"
	"os"
)

func main() {
	InitConfig()

	common.InitDB()

	r := gin.Default()
	r = CollectionRoute(r)

	port := viper.GetString("server.port")
	if port != "" {
		panic(r.Run(":" + port))
	}
	r.Run() // 默认监听并在 0.0.0.0:8080 上启动服务
}

// InitConfig 读取配置文件
func InitConfig() {
	workDir, _ := os.Getwd()
	viper.SetConfigName("application")
	viper.SetConfigType("yml")
	viper.AddConfigPath(workDir + "/config")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}
