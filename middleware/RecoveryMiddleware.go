package middleware

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"oceanlearn.tech/ginessential/response"
)

// RecoveryMiddleware 处理错误
func RecoveryMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				response.Fail(ctx, nil, fmt.Sprint(err))
			}
		}()
		ctx.Next()
	}
}
